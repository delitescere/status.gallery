# Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.

FROM azul/zulu-openjdk-alpine:11-jre-headless

EXPOSE 8080
WORKDIR /opt
ENTRYPOINT ["java", "-jar", "status.gallery.jar"]
ADD build/libs/status.gallery-*.jar /opt/status.gallery.jar
