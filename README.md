# [status.gallery](https://status.gallery/)

Copyright © 2021. Joshua A. Graham https://status.gallery/
See [LICENSE.txt](LICENSE.txt) for usage rights.

A reactive system written with Kotlin Coroutines and Kotlin Flows using Spring Boot and Hotwire.

# Documentation

See the [table of contents](docs/adr/toc.md) for the architecture decision records.

To update the table of contents, use:

```shell
bin/adrtoc
```

or `gradle adrTableOfContents`

An [explainer presentation](http://docs.status.gallery) with marble diagrams shows
how the Shared and State Flows work together.

# Running locally

## Pre-requisites

These are to install development-time pre-requisites.

```shell
bin/prereq
```

will install Homebrew and the packages specified in [`Brewfile`](Brewfile) and [`.tool-versions`](.tool-versions).

* [Homebrew](https://brew.sh) for system-wide tools
* [ASDF](https://asdf-vm.com) for project-specific version of a tool/SDK
* [nmap](https://nmap.org/ncat/) for the `ncat` utility (as BSD `nc` isn't necessarily available)

## Test

### IntelliJ

In
the [Preferences | Build, Execution, Deployment | Build Tools | Gradle](jetbrains://idea/settings?name=Build%2C+Execution%2C+Deployment--Build+Tools--Gradle)
tab, set both

* `Build and run using:` `IntelliJ IDEA`
* `Run tests using:` `IntelliJ IDEA`

Run the provided IDEA Configuration [All unit in status.test](.run/All%20in%20status.test.run.xml) for unit tests, with `Toggle Auto-Test` turned on
for continuous testing.

Run the provided IDEA Configuration [All integration in status.test](.run/All%20integration%20in%20status.test.run.xml) for integration tests.

### Gradle

```shell
# unit tests
gradle test
# integration tests
gradle integrationTest
# all tests
gradle testAll
```

## Run

### IntelliJ

1. Select the [`StatusApp`](.run/StatusApp.run.xml) IDEA Configuration
2. <kbd>^</kbd><kbd>R</kbd>

### Gradle

```shell
bin/dev
```

To see the app without progressive enhancement, you can disable JavaScript or use a textual browser. [w3m](http://w3m.sourceforge.net) respects the
"meta refresh" directive on the `/status` page.

Using the [sample](src/sample/observees.json) configuration file, try the following while looking at the page:

```shell
(sleep 1; rm src/sample/test.file; sleep 5; touch src/sample/test.file)
```

You'll see the file-based Observee go from Healthy, to Ailing, to Unhealthy, then back to Healthy.

The `bounceMinutes` [configuration](src/main/resources/application.yaml) value will force an Observee event to be emitted if there's been no status
change in that time. It can be set with the `APP_BOUNCE_MINUTES` environment variable. The default is to not bounce.

# CI

```shell
# unit and integration tests
gradle check

# build artifact
gradle bootJar

# launch for functional tests / example execution
APP_CONFIG_FILE=src/sample/observees.json java -jar build/libs/status-0.0.1.jar

# build Docker image
DOCKER_BUILDKIT=1 docker build -t status.gallery . 
```

# Docker

To build and run from IntelliJ, use the provided [Dockerfile](.run/Dockerfile.run.xml) configuration.

As an example:

```shell
docker run -p 8080:8080 -v $PWD/src/sample:/opt/src/sample:ro --env APP_CONFIG_FILE=src/sample/observees.json --name status.gallery --rm status.
gallery
```
