/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.io.FileOutputStream
import java.net.URL
import java.nio.channels.Channels

plugins {
    kotlin("jvm")
    kotlin("plugin.spring")
    idea
    jacoco

    id("org.jetbrains.gradle.plugin.idea-ext") version "1.0.1"
    id("com.coditory.integration-test") version "1.3.0"

    id("org.springframework.boot") version "2.5.4"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"

    `project-report`
}

group = "gallery.status"
version = "0.0.1"

val vendorDir = projectDir.resolve(".vendor")
val staticDir = buildDir.resolve("resources/main/static")

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(platform(kotlin("bom")))

    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")

    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("io.arrow-kt:arrow-core:0.13.2")

    /**
     * Log4J2, async logging, YAML config
     */
    runtimeOnly("org.apache.logging.log4j:log4j-jcl")  // implement Java Commons Logging
    runtimeOnly("org.apache.logging.log4j:log4j-1.2-api")  // implement Log4J legacy API
    runtimeOnly("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    runtimeOnly("com.fasterxml.jackson.core:jackson-core")
    runtimeOnly("com.lmax", "disruptor", "3.4.4")

    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-debug")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude("org.mockito")
    }
    testImplementation("com.ninja-squad:springmockk:3.0.1")
    testImplementation("io.mockk:mockk:1.12.0")
    testImplementation("org.mock-server:mockserver-junit-jupiter:5.11.2")

    developmentOnly("org.springframework.boot:spring-boot-devtools")
    developmentOnly("io.projectreactor:reactor-tools")
}

configurations.all {
    resolutionStrategy.dependencySubstitution {
        exclude("ch.qos.logback", "logback-classic")
        exclude("org.apache.logging.log4j", "log4j-to-slf4j")
        exclude("org.slf4j", "jul-to-slf4j")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict", "-Xopt-in=kotlin.time.ExperimentalTime")
        jvmTarget = java.targetCompatibility.toString()
    }
}

tasks.withType<Test> {
    systemProperty("spring.profiles.active", "dev,test")
    useJUnitPlatform()
    testLogging {
        events("failed")
    }
}

tasks.register("dev") {
    group = "application"
    description = "Runs the Spring Boot application with the dev profile"
    doFirst {
        tasks.bootRun.configure {
            systemProperty("logging.pattern", "%highlight{[%level ] %date{HH:mm:ss} %logger{3}: %msg%n%throwable}")
            systemProperty("spring.profiles.active", "dev")
        }
    }
    finalizedBy("bootRun")
}

tasks.jacocoTestReport {
    dependsOn(tasks.withType<Test>()) // tests are required to run before generating the report
    executionData(fileTree(project.buildDir).include("jacoco/*.exec"))
    reports {
        xml.required.set(true)
        csv.required.set(true)
        html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
    }
}

/**
 * Files are available in `.vendor` for IDEA build and Spring DevTools too
 */
val downloadBrowserDeps = tasks.register("downloadBrowserDeps") {
    doFirst {
        mkdir(vendor("scripts"))
        mkdir(vendor("stylesheets"))

        URL("https://unpkg.com/@hotwired/turbo").download(vendor("scripts/turbo.js"))
        URL("https://cdn.jsdelivr.net/npm/bootstrap/dist/js/bootstrap.min.js").download(vendor("scripts/"))
        URL("https://cdn.jsdelivr.net/npm/bootstrap/dist/js/bootstrap.min.js.map").download(vendor("scripts/"))
        URL("https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css").download(vendor("stylesheets/"))
        URL("https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css.map").download(vendor("stylesheets/"))
    }
}

val copyBrowserDeps = tasks.register<Copy>("copyBrowserDeps") {
    from(vendorDir)
    into(staticDir)

    dependsOn(downloadBrowserDeps)
}

tasks.processResources {
    dependsOn(copyBrowserDeps)
}

val updateAdrToc = tasks.register("adrTableOfContents") {
    doLast {
        exec {
            workingDir(projectDir)
            commandLine("bin/adrtoc")
        }
    }
}

fun vendor(path: String) = vendorDir.resolve(path)

fun URL.download(file: File) {
    val target = if (file.isDirectory) file.resolve(this.path.split('/').last()) else file
    val inputChannel = Channels.newChannel(openStream())
    val outputChannel = FileOutputStream(target).channel
    outputChannel.transferFrom(inputChannel, 0, Long.MAX_VALUE);
}
