/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.config

import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Test
import test.BaseTest
import java.net.URL
import kotlin.io.path.Path
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

@ExperimentalTime
internal class ObserveeJsonTest : BaseTest() {
    @Test
    fun `should ignore JSON array entry with no name`() {
        val json = ObserveeJson(null, URL("file:/"), null, period = "1s")
        assertThat(ObserveeJson.toObservees(Path(""), listOf(json)).toList()).isEmpty()
    }

    @Test
    fun `should ignore JSON array entry with no url`() {
        val json = ObserveeJson("name", null, null, period = "1s")
        assertThat(ObserveeJson.toObservees(Path(""), listOf(json)).toList()).isEmpty()
    }

    @Test
    fun `should treat bare numeric strings for period as minutes`() {
        val json = ObserveeJson("name", URL("file:/"), null, period = "1")
        assertThat(ObserveeJson.toObservees(Path(""), listOf(json)).first().with.period).isEqualTo(Duration.minutes(1))
    }
}
