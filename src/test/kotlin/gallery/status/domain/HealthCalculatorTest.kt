/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import arrow.core.right
import gallery.status.domain.event.ObserveUrlEvent
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import test.BaseTest
import java.net.URL
import java.time.*
import kotlin.time.ExperimentalTime

@Suppress("PrivatePropertyName")
@ExperimentalStdlibApi
@ExperimentalTime
class HealthCalculatorTest : BaseTest() {
    private val ZERO = 0u

    @Nested
    inner class InsufficientObservationsWithDegradeAndRecover {
        @Test
        fun `should be gray with none`() {
            val range = rangeWith(degrade = ZERO, recover = ZERO).dropLast(2)
            val calculator = HealthCalculator(range, 1u, 1u)
            assertThat(calculator.calculate()).isEqualTo(HealthStatus.Gray)
        }

        @Test
        fun `should be red with failure`() {
            val range = rangeWith(degrade = ZERO, recover = ZERO).dropLast(1)
            val calculator = HealthCalculator(range, 1u, 1u)
            assertThat(calculator.calculate()).isEqualTo(HealthStatus.Red)
        }

        @Test
        fun `should be green with success`() {
            val range = rangeWith(true, degrade = ZERO, recover = ZERO).dropLast(1)
            val calculator = HealthCalculator(range, 1u, 1u)
            assertThat(calculator.calculate()).isEqualTo(HealthStatus.Green)
        }

        @Test
        fun `should be red with success, failure`() {
            val range = rangeWith(true, degrade = ZERO, recover = ZERO)
            val calculator = HealthCalculator(range, 1u, 1u)
            assertThat(calculator.calculate()).isEqualTo(HealthStatus.Red)
        }

        @Test
        fun `should be amber with failure, success`() {
            val range = rangeWith(false, true, degrade = ZERO, recover = ZERO)
            val calculator = HealthCalculator(range, 1u, 1u)
            assertThat(calculator.calculate()).isEqualTo(HealthStatus.Amber)
        }
    }

    @Test
    fun `should sort range newest to oldest`() {
        val range = rangeWith(degrade = ZERO, recover = ZERO)
        val calculator = HealthCalculator(range.reversed(), ZERO, ZERO)
        assertThat(calculator.events).isEqualTo(range)
    }

    @Test
    fun `should be green when recover count 0 and success, any`() {
        val recover = 0u
        val range = rangeWith(true, degrade = ZERO, recover = recover)
        val calculator = HealthCalculator(range, ZERO, recover)
        assertThat(calculator.calculate()).isEqualTo(HealthStatus.Green)
    }

    @Test
    fun `should be green when degrade count 4 and only first and last success`() {
        val degrade = 4u
        val range = rangeWith(true, false, false, true, degrade = degrade, recover = ZERO)
        val calculator = HealthCalculator(range, degrade, ZERO)
        assertThat(calculator.calculate()).isEqualTo(HealthStatus.Green)
    }

    @Test
    fun `should be red when recover count 1 and success, failure, any`() {
        val recover = 1u
        val range = rangeWith(true, false, degrade = ZERO, recover = recover)
        val calculator = HealthCalculator(range, ZERO, recover)
        assertThat(calculator.calculate()).isEqualTo(HealthStatus.Red)
    }

    @Test
    fun `should be green when recover count 2 and success, success, success, failure, any`() {
        val recover = 2u
        val range = rangeWith(true, true, true, false, degrade = ZERO, recover = recover)
        val calculator = HealthCalculator(range, ZERO, recover)
        assertThat(calculator.calculate()).isEqualTo(HealthStatus.Green)
    }

    @Test
    fun `should be amber when degrade count 1 and failure, success, any`() {
        val degrade = 1u
        val range = rangeWith(false, true, degrade = degrade, recover = ZERO)
        val calculator = HealthCalculator(range, degrade, ZERO)
        assertThat(calculator.calculate()).isEqualTo(HealthStatus.Amber)
    }

    @Test
    fun `should be red when degrade count 1 and failure, failure, any`() {
        val degrade = 1u
        val range = rangeWith(false, false, degrade = degrade, recover = ZERO)
        val calculator = HealthCalculator(range, degrade, ZERO)
        assertThat(calculator.calculate()).isEqualTo(HealthStatus.Red)
    }

    @Test
    fun `should be amber when degrade count 2 and failure, failure, success, any`() {
        val degrade = 2u
        val range = rangeWith(false, false, true, degrade = degrade, recover = ZERO)
        val calculator = HealthCalculator(range, degrade, ZERO)
        assertThat(calculator.calculate()).isEqualTo(HealthStatus.Amber)
    }

    @Test
    fun `should be red when degrade count 2 and failure, failure, failure, any`() {
        val degrade = 2u
        val range = rangeWith(false, false, false, degrade = degrade, recover = ZERO)
        val c = HealthCalculator(range, degrade, ZERO)
        assertThat(c.calculate()).isEqualTo(HealthStatus.Red)
    }

    private fun rangeWith(vararg results: Boolean, degrade: DegradeBeyond = 0u, recover: RecoverBeyond = 0u): List<ObserveUrlEvent> {
        val top = results.asList()
        val degrades = degrade?.toLong() ?: 0
        val recovers = recover?.toLong() ?: 0
        val remainder = 1 + (degrades * 2) + recovers - top.size
        return buildList {
            for (i in top.indices) {
                val result: ObservationResult = if (top[i]) Observation.Success.right() else Observation.from(1u)
                add(ObserveUrlEvent(LocalDateTime.MAX.minusDays(i.toLong()).toInstant(ZoneOffset.UTC), URL("file:/"), result))
            }
            for (i in remainder downTo 0)
                add(ObserveUrlEvent(LocalDateTime.MIN.plusDays(i).toInstant(ZoneOffset.UTC), URL("file:/"), Observation.from(1u)))
        }
    }
}
