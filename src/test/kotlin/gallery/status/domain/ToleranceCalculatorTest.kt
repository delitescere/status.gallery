/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import test.BaseTest
import kotlin.time.ExperimentalTime

@Suppress("ClassName")
@ExperimentalTime
class ToleranceCalculatorTest : BaseTest() {

    @Nested
    inner class `with tolerance higher than number of instances` {
        private val tolerate = Tolerance.of(4u)

        @Test
        fun `should not tolerate all but 1 not being green or gray`() {
            val list = listOf(HealthStatus.Green, HealthStatus.Red, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Green)
        }

        @Test
        fun `should not tolerate all being red`() {
            val list = listOf(HealthStatus.Red, HealthStatus.Red, HealthStatus.Red)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Red)
        }
    }

    @Nested
    inner class `with tolerance of 0` {
        private val tolerate = Tolerance.default

        @Test
        fun `should be same status as instance if all are that status or gray`() {
            for (status in HealthStatus.values()) {
                val list = listOf(HealthStatus.Gray, status, status)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(status)
            }
        }

        @Test
        fun `should be amber if all are gray, green, but at least one is amber`() {
            val list = listOf(HealthStatus.Green, HealthStatus.Green, HealthStatus.Gray, HealthStatus.Amber)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Amber)
        }

        @Test
        fun `should be red if all are gray, green, orange but at least one is red`() {
            val list = listOf(HealthStatus.Red, HealthStatus.Green, HealthStatus.Gray, HealthStatus.Amber)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Red)
        }
    }

    @Nested
    inner class `with tolerance of 1` {
        private val tolerate = Tolerance.of(1u)

        @Test
        fun `should be same status as instance if all are that status or gray`() {
            for (status in HealthStatus.values()) {
                val list = listOf(HealthStatus.Gray, status, status)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(status)
            }
        }

        @Test
        fun `should be green if all are gray, green, but one is amber`() {
            val list = listOf(HealthStatus.Amber, HealthStatus.Green, HealthStatus.Green, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Green)
        }

        @Test
        fun `should be green if all are gray, green, but one is red`() {
            val list = listOf(HealthStatus.Red, HealthStatus.Green, HealthStatus.Green, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Green)
        }

        @Test
        fun `should be amber if all are gray, green, but two are amber`() {
            val list = listOf(HealthStatus.Amber, HealthStatus.Amber, HealthStatus.Green, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Amber)
        }

        @Test
        fun `should be amber if all are gray, green, but one is amber and one is red`() {
            val list = listOf(HealthStatus.Red, HealthStatus.Amber, HealthStatus.Green, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Amber)
        }

        @Test
        fun `should be red if all are gray, green, but two are amber and one is red`() {
            val list = listOf(HealthStatus.Amber, HealthStatus.Amber, HealthStatus.Red, HealthStatus.Green, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Amber)
        }

        @Test
        fun `should be red if all are gray, green, but two are red`() {
            val list = listOf(HealthStatus.Red, HealthStatus.Red, HealthStatus.Green, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Red)
        }

        @Test
        fun `should be red if all are gray, green, amber but two are red`() {
            val list = listOf(HealthStatus.Red, HealthStatus.Red, HealthStatus.Amber, HealthStatus.Green, HealthStatus.Gray)
            assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Red)
        }
    }

    @Nested
    inner class `with apparently total tolerance` {
        private val tolerate = Tolerance.pc(100u)

        @Nested
        internal inner class `with 1 instance` {
            @Test
            fun `should not be tolerant`() {
                for (status in HealthStatus.values()) {
                    val list = listOf(status)
                    assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(status)
                }
            }
        }

        @Nested
        internal inner class `with multiple instances` {
            @Test
            fun `should be green when green, green`() {
                val list = listOf(HealthStatus.Green, HealthStatus.Green)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Green)
            }

            @Test
            fun `should be green when green, amber`() {
                val list = listOf(HealthStatus.Green, HealthStatus.Amber)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Green)
            }

            @Test
            fun `should be green when green, red`() {
                val list = listOf(HealthStatus.Green, HealthStatus.Red)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Green)
            }

            @Test
            fun `should be amber when amber, amber`() {
                val list = listOf(HealthStatus.Amber, HealthStatus.Amber)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Amber)
            }

            @Test
            fun `should be amber when amber, red`() {
                val list = listOf(HealthStatus.Amber, HealthStatus.Red)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Amber)
            }

            @Test
            fun `should be red when red, red`() {
                val list = listOf(HealthStatus.Red, HealthStatus.Red)
                assertThat(ToleranceCalculator.calculate(tolerate, list)).isEqualTo(HealthStatus.Red)
            }
        }
    }
}
