/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain.flow

import gallery.status.domain.HealthStatus
import gallery.status.domain.Observation
import gallery.status.domain.Observee
import gallery.status.domain.event.ObserveeHealthEvent
import gallery.status.service.ObserveUrl
import io.mockk.coEvery
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.net.URL
import java.time.*
import kotlin.time.Duration
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.test.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ObserveeHealthFlowTest : test.BaseTest() {
    init {
        mockkObject(ObserveUrl)
    }

    @AfterEach
    fun checkCoroutines() {
        unmockkAll()
        scope.cleanupTestCoroutines()
    }

    private val scope = TestCoroutineScope()

    private val url = URL("file:/dev/null")
    private val period = Duration.seconds(1)
    private val tick = period.inWholeMilliseconds
    private val observee = Observee("observee1", url, with = Observee.Options(period = period))
    private val healthFlow = ObserveeHealthFlow(observee, scope)
    private val checkUrl = coEvery { ObserveUrl.check(url) }

    @Test
    fun `simulate Instance state flow calculating Observee state flow`() = scope.runBlockingTest {
        checkUrl.returns(Observation.success)

        var healthStatus = HealthStatus.Gray

        pauseDispatcher {
            scope.launch {
                healthFlow.state.collect {
                    healthStatus = it.healthStatus
                } // the flow only emits while subscribed
            }
            healthFlow.start()
            advanceTimeBy(tick)  // runs start function through second loop
            scope.launch {  // collects state flow at that point
                assertThat(healthStatus).isEqualTo(HealthStatus.Green)
                healthFlow.stop()  // stops start function loop and cancels its coroutines
            }
        }
    }

    @Test
    fun `simulate changes in Instance state flow calculating multiple Observee state flow`() = scope.runBlockingTest {
        checkUrl.returnsMany(Observation.success, Observation.from(1u))

        var healthStatus = HealthStatus.Gray

        pauseDispatcher {
            scope.launch {
                healthFlow.state.collect {
                    healthStatus = it.healthStatus
                } // the flow only emits while subscribed
            }
            healthFlow.start()
            advanceTimeBy(tick)  // runs start function through second loop
            scope.launch {  // collects state flow at that point
                assertThat(healthStatus).isEqualTo(HealthStatus.Red)
                healthFlow.stop()  // stops start function loop and cancels its coroutines
            }
        }
    }

    @Test
    fun `simulate bouncing Observee state flow after 1 minute`() = scope.runBlockingTest {
        val tick = 30L
        val events = mutableListOf<ObserveeHealthEvent>()
        val observee = Observee("observee1", url, with = Observee.Options(period = Duration.seconds(tick)))
        val healthFlow = ObserveeHealthFlow(observee, scope, Clock.fixed(Instant.EPOCH, ZoneId.of("UTC")), 1u)

        checkUrl.returns(Observation.success)

        pauseDispatcher {
            scope.launch {
                healthFlow.state.collect {
                    events.add(it)
                }
            }

            healthFlow.start()                      // 0:00 status=gray
            moveClockForward(healthFlow, tick)      // 0:30 status=green
            assertThat(events.size).isEqualTo(2)    // gray, green
            moveClockForward(healthFlow, tick)      // 1:00 status=green
            assertThat(events.size).isEqualTo(2)    // gray, green
            moveClockForward(healthFlow, tick)      // 1:30 status=green (bounce)
            assertThat(events.size).isEqualTo(3)    // gray, green, green bounced
            healthFlow.stop()
        }
    }

    private fun moveClockForward(healthFlow: ObserveeHealthFlow, seconds: Long) {
        healthFlow.clock = Clock.fixed(healthFlow.clock.instant().plusSeconds(seconds), ZoneId.systemDefault())
        scope.advanceTimeBy(seconds * 1000)
    }
}
