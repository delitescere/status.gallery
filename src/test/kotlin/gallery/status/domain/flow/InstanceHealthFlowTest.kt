/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain.flow

import gallery.status.domain.HealthStatus
import gallery.status.domain.Observation
import gallery.status.domain.Observee
import gallery.status.service.ObserveUrl
import io.mockk.coEvery
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.net.URL
import kotlin.time.Duration
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.test.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class InstanceHealthFlowTest : test.BaseTest() {
    init {
        mockkObject(ObserveUrl)
    }

    @AfterEach
    fun checkCoroutines() {
        unmockkAll()
        scope.cleanupTestCoroutines()
    }

    private val scope = TestCoroutineScope()

    private val url = URL("file:/dev/null")
    private val period = Duration.seconds(1)
    private val tick = period.inWholeMilliseconds
    private val observee = Observee("observee1", url, with = Observee.Options(period = period))
    private val healthFlow = ObserveeHealthFlow(observee, scope)
    private val checkUrl = coEvery { ObserveUrl.check(url) }

    @Test
    fun `simulate Instance shared flow calculating Instance state flow`() = scope.runBlockingTest {
        checkUrl.returns(Observation.success)

        var healthStatus = HealthStatus.Gray

        pauseDispatcher {
            scope.launch {
                healthFlow.state.collect {
                    healthStatus = it.healthStatus
                } // the flow only emits while subscribed
            }

            healthFlow.start()
            advanceTimeBy(tick)
            assertThat(healthStatus).isEqualTo(HealthStatus.Green)
            healthFlow.stop()  // stops start function loop and cancels its coroutines
        }
    }

    @Test
    fun `simulate changes in Instance shared flow calculating multiple Instance state flow`() = scope.runBlockingTest {
        checkUrl.returnsMany(Observation.success, Observation.from(1u))

        var healthStatus = HealthStatus.Gray

        pauseDispatcher {
            scope.launch {
                healthFlow.state.collect {
                    healthStatus = it.healthStatus
                } // the flow only emits while subscribed
            }

            healthFlow.start()
            advanceTimeBy(tick)  // runs start function through second loop
            assertThat(healthStatus).isEqualTo(HealthStatus.Red)
            healthFlow.stop()
        }
    }

    @Test
    fun `simulate resubscribe to flow`() = scope.runBlockingTest {
        checkUrl.returnsMany(Observation.success, Observation.success, Observation.from(1u))

        var healthStatus = HealthStatus.Gray
        var job: Job

        pauseDispatcher {
            job = launch {
                healthFlow.state.collect {
                    healthStatus = it.healthStatus
                }
            }
            healthFlow.start()
            advanceTimeBy(tick)
            job.cancel() // unsubscribe
            assertThat(healthStatus).isEqualTo(HealthStatus.Green)
            healthFlow.stop()

            job = launch {
                healthFlow.state.collect {
                    healthStatus = it.healthStatus
                }
            }
            healthFlow.start()
            advanceTimeBy(tick)
            job.cancel() // unsubscribe
            assertThat(healthStatus).isEqualTo(HealthStatus.Red)
            healthFlow.stop()
        }
    }
}

