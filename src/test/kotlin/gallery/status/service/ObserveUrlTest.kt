/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.service

import org.junit.jupiter.api.Test
import test.BaseTest
import test.EitherAssert.Companion.assertThat
import java.net.URL
import kotlinx.coroutines.*
import kotlin.time.ExperimentalTime

@ExperimentalTime
class ObserveUrlTest : BaseTest() {
    @Test
    fun `existing file should succeed`() {
        assertThat(ObserveUrl.blocking(URL("file:/dev/null"), 1u)).isRight
    }

    @Test
    fun `non-existing file should fail`() {
        assertThat(ObserveUrl.blocking(URL("file:?"), 1u)).isLeft
    }

    @Test
    fun `non-existing ftp should fail`() {
        assertThat(ObserveUrl.blocking(URL("ftp:/"), 1u)).isLeft
    }

    @Test
    fun `non-existing http should fail`() {
        assertThat(ObserveUrl.blocking(URL("http:/"), 1u)).isLeft
    }

    @Test
    fun `coroutine should succeed`() {
        assertThat(runBlocking { ObserveUrl.check(URL("file:/dev/null"), 1u) }).isRight
    }
}
