/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package test

import arrow.core.Either
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.apache.logging.log4j.core.LoggerContext
import org.apache.logging.log4j.core.appender.ConsoleAppender
import org.apache.logging.log4j.core.config.Configuration
import org.apache.logging.log4j.core.layout.PatternLayout
import org.assertj.core.api.AbstractAssert
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.time.ExperimentalTime

@ExperimentalTime
abstract class BaseTest(rootLogLevel: Level? = Level.OFF) {
    init {
        val ctx: LoggerContext = LogManager.getContext(false) as LoggerContext
        val config: Configuration = ctx.configuration
        config.rootLogger.level = rootLogLevel

        val layout = PatternLayout.newBuilder()
            .withConfiguration(config)
            .withPattern("%highlight{[%level ] %date{HH:mm:ss,SSS} [%threadName] %logger: %msg%n%throwable}")
            .withNoConsoleNoAnsi(!System.getProperty("console.ansi", "false").toBoolean())
            .build()

        val oldConsole = config.appenders.values.find { it is ConsoleAppender }
        oldConsole?.apply { stop() }?.apply { config.rootLogger.removeAppender(name) }
        val newConsole = ConsoleAppender.createDefaultAppenderForLayout(layout)
        newConsole.start()
        config.rootLogger.addAppender(newConsole, config.rootLogger.level, config.rootLogger.filter)
    }

    @Suppress("PropertyName")
    val LOG: Logger = LogManager.getLogger(this.javaClass)
}

@ExperimentalTime
@ExtendWith(SpringExtension::class)
abstract class SpringTest : BaseTest()


class EitherAssert(actual: Either<Any, Any>) :
    AbstractAssert<EitherAssert?, Either<Any, Any>>(actual, EitherAssert::class.java) {

    val isLeft get() = assertIsLeft()
    val isRight get() = assertIsRight()

    private fun assertIsLeft(): EitherAssert {
        isNotNull
        if (!actual.isLeft()) {
            actual as Either.Right
            failWithMessage("Expected an Either.Left, but got a ${actual.value.javaClass.canonicalName}\n\tvalue: ${actual.value}")
        }
        return this
    }

    private fun assertIsRight(): EitherAssert {
        isNotNull
        if (!actual.isRight()) {
            actual as Either.Left
            failWithMessage("Expected an Either.Right, but got a ${actual.value.javaClass.canonicalName}\n\tvalue: ${actual.value}")
        }
        return this
    }

    companion object {
        fun assertThat(actual: Either<Any, Any>): EitherAssert {
            return EitherAssert(actual)
        }
    }
}
