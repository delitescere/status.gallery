/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.service

import gallery.status.domain.Observee
import gallery.status.domain.flow.ObserveeHealthFlow
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import kotlin.time.ExperimentalTime

@ExperimentalTime
@Service
class ObserveeSupervisor @Autowired constructor(private val observees: List<Observee>, private val scope: CoroutineScope) : AutoCloseable {
    private val log = LogManager.getLogger()

    @Value("\${app.bounceMinutes:#{null}}")
    private var bounceMinutes: String? = null

    internal lateinit var observeeHealthFlows: List<ObserveeHealthFlow>
    private var closed = false

    suspend fun open(): ObserveeSupervisor {
        log.info("$this open with $observees")
        val bounce = bounceMinutes?.runCatching { toUInt() }?.getOrNull()?.also { log.info("Bouncing every $it minutes") }

        observeeHealthFlows = observees.map { observee ->
            ObserveeHealthFlow(observee, scope, bounce).also { observeeState ->
                scope.launch {
                    observeeState.start()
                }
            }
        }

        return this
    }

    override fun close() {
        if (!closed) {
            closed = true
            observeeHealthFlows.forEach { it.stop() }
        } else log.trace("$this already closed")
    }
}
