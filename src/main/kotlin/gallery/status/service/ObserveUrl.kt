/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.service

import arrow.core.Either
import gallery.status.domain.Observation
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.net.UrlConnectionFactory
import org.springframework.stereotype.Service
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.URL
import kotlinx.coroutines.*

@Service
/**
 * Because Connection instances can't be reliably reused after closing, this is an Object with no state.
 * TODO: record latency
 * TODO: perhaps look at using a low-level Socket instead
 */
object ObserveUrl {
    private val log = LogManager.getLogger()

    /**
     * Executes in [Dispatchers.IO], defaulting to [Observation.defaultTimeout] milliseconds
     */
    suspend fun check(url: URL, timeout: UInt? = Observation.defaultTimeout): Either<Observation.Error, Observation.Success> = Dispatchers.IO {
        blocking(url, timeout).also {
            log.debug("$url result $it")
        }
    }

    /**
     * For testing in an enclosing coroutine context
     */
    internal fun blocking(url: URL, timeout: UInt? = Observation.defaultTimeout): Either<Observation.Error, Observation.Success> {
        val conn = UrlConnectionFactory.createConnection(url)
        val http: HttpURLConnection? =
            if (url.protocol == "https" || url.protocol == "http") conn as HttpURLConnection else null
        val timeoutUsed = (timeout ?: Observation.defaultTimeout).coerceAtMost(Observation.maxTimeout)

        try {
            conn.apply {
                allowUserInteraction = false
                connectTimeout = timeoutUsed.toInt()
                readTimeout = timeoutUsed.toInt()
                useCaches = false
            }.apply {
                http?.apply { requestMethod = "HEAD" }
            }.run {
                connect()
                http?.apply {
                    disconnect()
                }
                ignore { getOutputStream().close() }
                ignore { getInputStream().close() }
                return Observation.from(this)
            }
        } catch (e: Exception) {
            return when (e) {
                is SocketTimeoutException -> Observation.from(timeoutUsed)
                else -> Observation.from(e)
            }
        }
    }

    private fun <T> ignore(f: () -> T): T? = try {
        f()
    } catch (e: Exception) {
        log.trace("ignoring", e)
        null
    }
}
