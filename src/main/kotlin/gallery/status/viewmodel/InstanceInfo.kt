/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.viewmodel

import gallery.status.domain.HealthStatus
import gallery.status.domain.event.InstanceHealthEvent
import gallery.status.domain.flow.InstanceHealthFlow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.net.URI
import java.time.Instant
import kotlin.time.ExperimentalTime

@ExperimentalTime
class InstanceInfo(
    id: Int,
    instant: Instant,
    healthStatus: HealthStatus,
    val uri: URI,
    bounceInstant: Instant? = null,
) : AbstractStatusInfo(id, instant, healthStatus, bounceInstant) {

    companion object {
        fun from(ev: InstanceHealthEvent) = InstanceInfo(ev.instance.hashCode(), ev.time, ev.healthStatus, ev.instance.uri)

        fun from(flow: InstanceHealthFlow): Flow<InstanceInfo> = flow.state.map(::from)
    }
}
