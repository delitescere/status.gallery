/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.viewmodel

import gallery.status.domain.HealthStatus
import gallery.status.domain.event.ObserveeHealthEvent
import gallery.status.domain.flow.ObserveeHealthFlow
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.map
import java.net.URL
import java.time.Instant
import kotlin.time.ExperimentalTime

@ExperimentalTime
class ObserveeInfo(
    id: Int,
    instant: Instant,
    healthStatus: HealthStatus,
    val label: String,
    val location: URL,
    val count: Int,
    bounceInstant: Instant? = null,
) : AbstractStatusInfo(id, instant, healthStatus, bounceInstant) {

    companion object {
        fun from(ev: ObserveeHealthEvent): ObserveeInfo =
            ObserveeInfo(ev.observee.hashCode(), ev.time, ev.healthStatus, ev.observee.label, ev.observee.location, ev.observee.instances.size, ev.bounceInstant)

        fun from(flows: List<ObserveeHealthFlow>): List<ObserveeInfo> = flows.map { it.state.value }.map(::from)

        @FlowPreview
        fun from(flows: Flow<ObserveeHealthFlow>): Flow<ObserveeInfo> = flows.flatMapMerge { it.state.map(::from) }

        fun from(flow: ObserveeHealthFlow): Flow<ObserveeInfo> = flow.state.map(::from)
    }
}
