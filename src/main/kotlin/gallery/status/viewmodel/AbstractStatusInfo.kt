/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.viewmodel

import gallery.status.domain.HealthStatus
import java.time.*
import java.time.format.*

abstract class AbstractStatusInfo(
    val id: Int,
    instant: Instant,
    val healthStatus: HealthStatus,
    bounceInstant: Instant?,
) {
    private val key = healthStatus.toString()
    private val healthStatusKeys = HealthStatus.values().map { it.toString() }
    private val instantFormatter = DateTimeFormatter
        .ofPattern("HH:mm:ss")
        .withZone(ZoneId.of("UTC"))

    /**
     * Values are based on [Bootcamp theme colors](https://getbootstrap.com/docs/5.1/customize/color/)
     */
    private val healthStatusAlerts = healthStatusKeys.zip(listOf("success", "danger", "warning", "secondary")).toMap()
    private val healthStatusNames =
        healthStatusKeys.zip(listOf("Healthy", "Unhealthy", "Ailing", "Unknown")).toMap()  // TODO: i18n
    private val healthStatusColors = healthStatusKeys.zip(listOf("green", "red", "amber", "gray")).toMap()

    val alert = healthStatusAlerts[key]
    val status = healthStatusNames[key]
    val color = healthStatusColors[key]
    val hhmmss = instantFormatter.format(instant)
    val timestamp = DateTimeFormatter.ISO_INSTANT.format(instant)
    val timestampBounce: String = bounceInstant?.let { DateTimeFormatter.ISO_INSTANT.format(it) } ?: ""
}
