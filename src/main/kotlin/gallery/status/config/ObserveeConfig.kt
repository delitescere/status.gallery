/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.config

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import gallery.status.domain.Observee
import gallery.status.domain.Tolerance
import gallery.status.service.ObserveeSupervisor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationFailedEvent
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.net.URI
import java.net.URL
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.math.absoluteValue
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

@ExperimentalTime
@Configuration
class ObserveeConfig {
    @Value("\${app.configFile}")
    private lateinit var configFile: String

    private val log = LogManager.getLogger()

    @Bean
    fun observees(): List<Observee> {
        val path = Path(configFile)
        return path.runCatching { file(this) }.getOrElse { fake() }
    }

    @Bean
    fun coroutineScope() = CoroutineScope(Dispatchers.Default)

    private fun fake(): List<Observee> {
        log.warn("Using fake observees")
        return listOf(
            Observee("/dev/null", URL("file:///dev/null"), with = Observee.Options(period = Duration.seconds(1)))
        )
    }

    private fun file(path: Path): List<Observee> = jacksonObjectMapper().runCatching {
        this.readValue<List<ObserveeJson>>(path.toFile())
    }.recoverCatching {
        log.error(it)
        throw it
    }.getOrThrow().let {
        ObserveeJson.toObservees(path, it).toList()
    }
}

/**
 * Starts the [ObserveeSupervisor] when the application is ready.
 * Closes the supervisor if the application fails.
 */
@ExperimentalTime
@Component
class ObservationStarter @Autowired constructor(private val supervisor: ObserveeSupervisor) {
    private val log = LogManager.getLogger()

    @EventListener(value = [ApplicationReadyEvent::class])
    fun onApplicationReady() {
        runBlocking(Dispatchers.Unconfined) {
            launch {
                supervisor.open()
            }
        }
    }

    @EventListener(value = [ApplicationFailedEvent::class])
    fun onApplicationFailed() {
        log.error("Application failed")
        supervisor.close()
    }
}

/**
 * @param tolerance whole unsigned numeric string, e.g. `"1"`, or whole unsigned percentage `"25%"`
 * @param period a string parseable as a [Duration] (or, if a bare numeric string, a trailing `"m"` is added for minutes)
 * @param timeout whole unsigned numeric string. Uses a sensible default if not specified.
 */
@ExperimentalTime
data class ObserveeJson(
    val name: String?,
    val url: URL?,
    val instances: List<URI>? = null,
    val tolerance: String? = null,
    val period: String? = null,
    val degrade: Int? = null,
    val recover: Int? = null,
    val timeout: Int? = null,
) {
    override fun toString(): String = listOfNotNull(
        the(name, "name"), the(url, "url"), the(instances, "instances"), the(tolerance, "tolerance"), the(period, "period"), the(degrade, "degrade"), the(recover, "recover"), the(timeout, "timeout")
    ).joinToString(", ")

    /**
     * Although the JSON deserializer has nullable fields, entries with null name or url must have been skipped before calling this.
     * @see toObservees
     */
    fun toObservee() = Observee(
        name!!,
        url!!,
        instances,
        Tolerance.parse(tolerance),
        Observee.Options(period?.runCatching { Duration.parse(this) }?.getOrNull() ?: Observee.Options.DEFAULT_PERIOD, degrade?.toUInt(), recover?.toUInt(), timeout?.toUInt())
    )

    companion object {
        private val log = LogManager.getLogger()

        /**
         * Filters out entries with missing name or url
         * @see Observee
         */
        fun toObservees(path: Path, json: List<ObserveeJson>) = json.filterNot { entry ->
            (entry.name == null || entry.url == null).also {
                if (it) log.warn("Observee configuration file ${path.toAbsolutePath()} invalid entry missing name: '${entry.name}' or url: '${entry.url}'")
            }
        }.map { observee ->
            val period = observee.period?.runCatching {
                "${toInt().absoluteValue}m" // treat bare numeric string as "minutes"
            }?.getOrDefault(observee.period)
            observee.copy(period = period)
        }.map { log.info(it); it.toObservee() }

        private fun the(field: Any?, label: String): String? = if (field != null) "$label=$field" else null
    }
}
