/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import gallery.status.domain.event.ObserveUrlEvent
import org.apache.logging.log4j.LogManager

class HealthCalculator
/**
 * @throws IllegalArgumentException when size of range is less than (degrade * 2) + recover + 1
 */
internal constructor(
    range: List<ObserveUrlEvent>,
    degrade: DegradeBeyond,
    recover: RecoverBeyond,
) {
    private val log = LogManager.getLogger()
    internal val events: List<ObserveUrlEvent> = range.sortedByDescending { it.time }
    private val degrade = degrade?.toInt() ?: 0
    private val recover = recover?.toInt() ?: 0

    /**
     * @return HealStatus based on most recent observations, using degrade and recover counts
     */
    internal fun calculate(): HealthStatus {
        if (events.isEmpty()) return HealthStatus.Gray

        val latest = events[0]
        val previousSuccess = events.drop(1).takeWhile { !it.success }.size + 1
        val previousFailure = events.drop(1).takeWhile { it.success }.size + 1
        val recovered = recover == 0 || previousFailure > recover || events.size <= recover
        val degraded = previousFailure <= events.size && previousSuccess <= degrade * 2 && previousSuccess < events.size

        return when {
            latest.success && recovered -> HealthStatus.Green
            latest.success && recover > 0 -> HealthStatus.Red
            latest.success -> HealthStatus.Amber
            degraded -> HealthStatus.Amber
            else -> HealthStatus.Red
        }
    }

    companion object {
        fun calculate(range: List<ObserveUrlEvent>, degrade: DegradeBeyond, recover: RecoverBeyond): HealthStatus =
            HealthCalculator(range, degrade, recover).calculate()
    }
}
