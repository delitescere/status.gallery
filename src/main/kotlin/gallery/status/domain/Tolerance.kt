/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import kotlin.math.roundToInt

sealed class Tolerance {
    class Count(val count: UInt) : Tolerance()
    class Percent(val percent: UInt) : Tolerance()

    /**
     * @return The minimum of `total - 1`, and either the tolerance `count` or the
     * tolerance `percent`age of the given `total` (rounded-half to the nearest integer).
     */
    fun onTotal(total: UInt): UInt {
        val totalOrZero = maxOf(0, total.toInt() - 1).toUInt()
        return when (this) {
            is Count -> minOf(count, totalOrZero)
            is Percent -> {
                val countFromPercent = ((total * percent).toDouble() / 100.0).roundToInt().toUInt()
                minOf(countFromPercent, totalOrZero)
            }
        }
    }

    companion object {
        /**
         * [UInt.MIN_VALUE] (i.e. `0`)
         */
        val default: Tolerance = Count(UInt.MIN_VALUE)
        fun of(count: Int) = Count(count.toUInt())
        fun of(count: UInt) = Count(count)
        fun pc(percent: Int) = Percent(percent.toUInt())
        fun pc(percent: UInt) = Percent(percent)

        /**
         * @param value an integer string (e.g. "2"), or integer percentage string (e.g. "10%")
         * @return [Tolerance.default] by default or if numeric conversion fails
         * @see Tolerance.default
         * @see Tolerance.Count
         * @see Tolerance.Percent
         */
        fun parse(value: String?): Tolerance {
            if (value == null) return default
            try {
                return when {
                    value.contains('%') -> pc(value.substringBefore('%').trim().toUInt())
                    else -> of(value.trim().toUInt())
                }
            } catch (_: NumberFormatException) {
            }
            return default
        }

    }
}
