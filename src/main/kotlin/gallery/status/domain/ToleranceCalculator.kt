/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import org.apache.logging.log4j.LogManager

object ToleranceCalculator {
    private val log = LogManager.getLogger()

    fun calculate(tolerate: Tolerance, instanceStatus: List<HealthStatus>): HealthStatus {
        val total = instanceStatus.size.toUInt()
        if (total == 0u) return HealthStatus.Gray

        val tolerance = tolerate.onTotal(total).toInt()
        val gray = instanceStatus.count { it == HealthStatus.Gray }
        val green = instanceStatus.count { it == HealthStatus.Green }
        val amber = instanceStatus.count { it == HealthStatus.Amber }
        val red = instanceStatus.count { it == HealthStatus.Red }

        return when {
            red > tolerance -> HealthStatus.Red
            (amber + red) > tolerance -> HealthStatus.Amber
            tolerance > 0 && amber - gray - green == tolerance -> HealthStatus.Amber
            instanceStatus.any { it == HealthStatus.Green } -> HealthStatus.Green
            else -> HealthStatus.Gray
        }
    }

}
