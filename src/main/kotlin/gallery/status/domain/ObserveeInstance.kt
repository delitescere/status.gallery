/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import java.net.URI
import java.net.URL
import kotlin.time.ExperimentalTime

@ExperimentalTime
// TODO: content model to obtain uptime, version, revision (git sha) via ObserveUrl
data class ObserveeInstance(val observee: Observee, val uri: URI) {
    val url: URL = uri.toURL()  // throw if unable to convert to URL
}
