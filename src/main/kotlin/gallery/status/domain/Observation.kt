/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import arrow.core.Either
import arrow.core.right
import java.net.HttpURLConnection
import java.net.URLConnection

object Observation {
    private val timeoutProperty: String = System.getProperty("observation.timeout", "1000")
    val maxTimeout = 10000u
    val defaultTimeout = timeoutProperty.runCatching { toUInt() }.getOrDefault(1000u).coerceAtMost(maxTimeout)
    val success = Success.right()

    fun from(timeout: UInt): ObservationResult {
        return Either.Left(Error.Timeout(timeout))
    }

    fun from(e: Exception): ObservationResult {
        return Either.Left(Error.Other(e))
    }

    fun from(conn: URLConnection): ObservationResult {
        val http: HttpURLConnection? =
            if (conn.url?.protocol == "https" || conn.url?.protocol == "http") conn as HttpURLConnection else null

        return http?.let {
            if (it.responseCode >= 500)
                Either.Left(Error.Http(it.responseCode, it.responseMessage))
            else
                success
        } ?: success
    }

    object Success
    sealed interface Error {
        data class Http(val code: Int, val message: String) : Error
        data class Timeout(val timeout: UInt) : Error
        data class Other(val cause: Exception) : Error
    }
}

