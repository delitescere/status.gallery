/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain.flow

import gallery.status.domain.HealthCalculator
import gallery.status.domain.HealthStatus
import gallery.status.domain.ObserveeInstance
import gallery.status.domain.event.InstanceHealthEvent
import gallery.status.domain.event.ObserveUrlEvent
import gallery.status.service.ObserveUrl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import java.time.Clock
import java.time.Instant
import kotlin.time.ExperimentalTime

/**
 * The health flow for an [ObserveeInstance].
 * Contains a private (hot) [SharedFlow] of [ObserveUrlEvent]s for HealthCalculator calculations.
 * Supplies a StateFlow of [InstanceHealthEvent]s for ToleranceCalculator calculations.
 */
@ExperimentalTime
class InstanceHealthFlow internal constructor(private val instance: ObserveeInstance, private val scope: CoroutineScope, private var clock: Clock) {
    constructor(instance: ObserveeInstance, scope: CoroutineScope) : this(instance, scope, Clock.systemUTC())

    private val log = LogManager.getLogger()
    private var stopped = false
    private val observee = instance.observee
    private val url = instance.url
    private val degrade = observee.with.degrade ?: 0u
    private val recover = observee.with.recover ?: 0u
    private val replay: Int get() = (1u + (degrade * 2u) + recover).toInt()
    private val _observeUrlFlow = MutableSharedFlow<ObserveUrlEvent>(replay, 0, BufferOverflow.DROP_OLDEST)
    private val initialState = InstanceHealthEvent(clock.instant(), observee, instance, HealthStatus.Gray)
    private val _stateFlow = MutableStateFlow(initialState)
    val state = _stateFlow.asStateFlow()

    init {
        log.debug("$instance created")
    }

    suspend fun start() {
        log.info("$instance started")
        scope.launch {

            while (!stopped) {
                val result = ObserveUrl.check(url, observee.with.timeout)
                val now = clock.instant()

                /*
                 * Update private flow with each ObserveUrlEvent.
                 * Acts as a wait-free lock-free ring buffer.
                 */
                _observeUrlFlow.emit(ObserveUrlEvent(now, url, result))

                /*
                 * Update published flow with each InstanceHealthEvent.
                 * Health is calculated based on the ordered ObserveUrlEvents captured in the private flow.
                 */
                emit(calculateHealth(now))

                delay(observee.with.period)
            }
        }
    }

    fun stop() {
        if (!stopped) {
            stopped = true
            _stateFlow.update { initialState }
            log.info("$instance stopped")
        } else log.trace("$instance already stopped")
    }

    private fun calculateHealth(now: Instant): InstanceHealthEvent = InstanceHealthEvent(now, observee, instance, HealthCalculator.calculate(_observeUrlFlow.replayCache, degrade, recover))

    private fun emit(event: InstanceHealthEvent) {
        if (!_stateFlow.tryEmit(event)) log.error("Unable to emit $event")

        /* TODO: create logger for un-healthy and re-healthy changes (to optionally feed to monitoring/alerting system)
         *
         *  if (healthCalculation == HealthStatus.Red) healthLogger.error("${observee.label} is unhealthy")
         *  if (healthCalculation == HealthStatus.Green) healthLogger.info("${observee.label} is healthy again")
         */
    }
}
