/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain.flow

import gallery.status.domain.HealthStatus
import gallery.status.domain.Observee
import gallery.status.domain.ToleranceCalculator
import gallery.status.domain.event.ObserveeHealthEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import java.time.Clock
import java.time.Instant
import kotlin.time.ExperimentalTime

@ExperimentalTime
class ObserveeHealthFlow internal constructor(private val observee: Observee, private val scope: CoroutineScope, internal var clock: Clock, private val bounceMinutes: UInt? = null) {
    constructor(observee: Observee, scope: CoroutineScope, bounceMinutes: UInt? = null) : this(observee, scope, Clock.systemUTC(), bounceMinutes)

    private val log = LogManager.getLogger()
    private var stopped = false
    private val initialState = ObserveeHealthEvent(clock.instant(), observee, HealthStatus.Gray)
    private val _stateFlow = MutableStateFlow(initialState)
    internal lateinit var instanceHealthFlows: List<InstanceHealthFlow>
    val state = _stateFlow.asStateFlow()

    init {
        log.info("$observee created")
    }

    suspend fun start(): ObserveeHealthFlow {
        log.debug("$observee started")

        instanceHealthFlows = observee.instances.map { instance ->
            InstanceHealthFlow(instance, scope).also {
                scope.launch {
                    it.start()
                }
            }
        }

        subscribeToInstanceFlows()
        return this
    }

    fun stop() {
        if (!stopped) {
            instanceHealthFlows.forEach { it.stop() }
            _stateFlow.update { initialState }
            log.info("$observee stopped")
        } else log.trace("$observee already stopped")
    }

    private fun subscribeToInstanceFlows() = instanceHealthFlows.forEach { instanceHealth ->
        scope.launch {
            instanceHealth.state.collectLatest { calculateHealth()?.also { emit(it) } }
        }
    }

    private fun currentInstanceHealth() = instanceHealthFlows.map { it.state.value.healthStatus }

    private fun calculateHealth(): ObserveeHealthEvent? = ToleranceCalculator.calculate(observee.tolerate, currentInstanceHealth()).let {
        val now = clock.instant()
        when {
            // only create an event when there's a change in status
            it != state.value.healthStatus -> ObserveeHealthEvent(now, observee, it)
            // or if a bounce is needed (which doesn't alter original event, just the bounceInstant)
            timeToBounce(now) -> ObserveeHealthEvent(state.value.time, observee, state.value.healthStatus, now)
            else -> null
        }
    }

    /**
     * If `bounceMinutes` or more have passed since the last related event
     * (whether the original event or a bounce of that event), it's time to bounce that event
     * (which will be a re-emission of the event with an updated bounceInstant).
     */
    private fun timeToBounce(now: Instant): Boolean = bounceMinutes?.let {
        _stateFlow.value.time.coerceAtLeast(_stateFlow.value.bounceInstant ?: Instant.EPOCH).plusSeconds(it.toLong() * 60)
    }?.let {
        it <= now
    } == true

    private fun emit(event: ObserveeHealthEvent) {
        if (!_stateFlow.tryEmit(event)) log.error("Unable to emit $event")

        /* TODO: create logger for un-healthy and re-healthy changes (to optionally feed to monitoring/alerting system)
         *
         *  if (healthCalculation == HealthStatus.Red) healthLogger.error("${observee.label} is unhealthy")
         *  if (healthCalculation == HealthStatus.Green) healthLogger.info("${observee.label} is healthy again")
         */
    }
}
