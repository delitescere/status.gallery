/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain.event

import gallery.status.domain.HealthStatus
import gallery.status.domain.Observee
import java.time.*
import kotlin.time.ExperimentalTime

@ExperimentalTime
data class ObserveeHealthEvent(val time: Instant, val observee: Observee, val healthStatus: HealthStatus, val bounceInstant: Instant? = null)
