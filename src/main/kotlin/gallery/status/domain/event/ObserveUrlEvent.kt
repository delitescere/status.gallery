/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain.event

import gallery.status.domain.ObservationResult
import java.net.URL
import java.time.*

data class ObserveUrlEvent(val time: Instant, val url: URL, val result: ObservationResult) {
    val success: Boolean get() = result.isRight()
}
