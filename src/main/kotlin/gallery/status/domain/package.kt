/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import arrow.core.Either

/**
 * Number of success observations after one success observation before considered Healthy again
 */
typealias RecoverBeyond = UInt?

/**
 * Number of failure observations after one failure observation before considered Ailing, and double that Unhealthy
 */
typealias DegradeBeyond = UInt?

typealias ObservationResult = Either<Observation.Error, Observation.Success>
