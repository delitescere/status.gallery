/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

import gallery.status.domain.Observee.Options.Companion.DEFAULT_PERIOD
import java.net.URI
import java.net.URL
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

//TODO: tree of observees
@ExperimentalTime
class Observee(
    /**
     * A short name for the observed system
     */
    val label: String,
    /**
     * The URL of the observed system (used as default instance if none supplied)
     */
    val location: URL,
    /**
     * List of URIs of instances monitored to assess overall health of the observed system.
     */
    instances: List<URI>? = null,
    /**
     * Number of instances that can be in a less healthy state before reducing the overall health status
     */
    internal val tolerate: Tolerance = Tolerance.default,
    internal val with: Options = Options(),
) {
    internal val instances: List<ObserveeInstance> = (instances ?: listOf(location.toURI())).map {
        ObserveeInstance(this, it)
    }

    override fun toString() = label

    data class Options(
        /**
         * Duration between monitoring events of each instance. Defaults to [DEFAULT_PERIOD]
         */
        val period: Duration = DEFAULT_PERIOD,
        /**
         * Number of failure monitoring events in a row an instance will be in an Ailing state before becoming Unhealthy
         */
        val degrade: UInt? = UInt.MIN_VALUE,
        /**
         * Number of success monitoring events in a row an instance will remain in an Unhealthy state before becoming Healthy
         */
        val recover: UInt? = UInt.MIN_VALUE,
        /**
         * Number of milliseconds before a connection or read operation will time out. Defaults to [Observation.defaultTimeout].
         * Maximum value is arbitrarily [Observation.maxTimeout].
         */
        val timeout: UInt? = Observation.defaultTimeout,
    ) {
        companion object {
            /**
             * 1 minute (60 seconds)
             */
            val DEFAULT_PERIOD = Duration.minutes(1)
        }
    }
}
