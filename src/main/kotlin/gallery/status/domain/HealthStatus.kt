/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.domain

/**
 * Available health status values
 */
enum class HealthStatus {
    Green,
    Red,
    Amber,
    Gray;

    override fun toString(): String {
        return name.lowercase()
    }
}
