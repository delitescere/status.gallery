/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status

import org.springframework.http.MediaType

class CustomMediaType(type: String) : MediaType(type) {
    companion object {
        const val TURBO_STREAM_VALUE = "text/vnd.turbo-stream.html"
        val TURBO_STREAM = MediaType(
            TURBO_STREAM_VALUE.split('/')[0],
            TURBO_STREAM_VALUE.split('/')[1]
        )
    }
}
