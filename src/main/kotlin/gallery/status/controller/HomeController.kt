/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.controller

import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
internal class HomeController {

    @GetMapping("/", produces = [MediaType.TEXT_HTML_VALUE])
    suspend fun index(model: Model): String {
        return "index"
    }
}
