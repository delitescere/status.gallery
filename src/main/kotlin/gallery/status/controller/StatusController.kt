/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.controller

import gallery.status.CustomMediaType
import gallery.status.service.ObserveeSupervisor
import gallery.status.viewmodel.InstanceInfo
import gallery.status.viewmodel.ObserveeInfo
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable
import kotlin.time.ExperimentalTime

@FlowPreview
@ExperimentalTime
@Controller
internal class StatusController @Autowired constructor(private val supervisor: ObserveeSupervisor) {
    @GetMapping("/status", produces = [MediaType.TEXT_HTML_VALUE])
    suspend fun index(model: Model): String {
        val observees = ObserveeInfo.from(supervisor.observeeHealthFlows)
        model.addAttribute("observees", observees)
        return "status"
    }

    /**
     * Stream of change to health status of any Observee
     */
    @GetMapping("/status.stream", produces = [MediaType.TEXT_EVENT_STREAM_VALUE, CustomMediaType.TURBO_STREAM_VALUE])
    suspend fun stream(model: Model): String {
        val observees = ObserveeInfo.from(supervisor.observeeHealthFlows.asFlow())
        model.addAttribute("observees", dataDrivenEach(observees))
        return "observee-status.turbo-stream"
    }

    /**
     * Stream of change to health status of specific Observee
     */
    @GetMapping("/observee/{observeeNumber}/status.stream", produces = [MediaType.TEXT_EVENT_STREAM_VALUE, CustomMediaType.TURBO_STREAM_VALUE])
    suspend fun stream(@PathVariable observeeNumber: Int, model: Model): String {
        val observees = ObserveeInfo.from(supervisor.observeeHealthFlows[observeeNumber])
        model.addAttribute("observees", dataDrivenEach(observees))
        return "observee-status.turbo-stream"
    }

    /**
     * Stream of change to health status of specific Instance of specific Observee
     */
    @GetMapping("/observee/{observeeNumber}/instance/{instanceNumber}/status.stream", produces = [MediaType.TEXT_EVENT_STREAM_VALUE, CustomMediaType.TURBO_STREAM_VALUE])
    suspend fun stream(@PathVariable observeeNumber: Int, @PathVariable instanceNumber: Int, model: Model): String {
        val instances = InstanceInfo.from(supervisor.observeeHealthFlows[observeeNumber].instanceHealthFlows[instanceNumber])
        model.addAttribute("instances", dataDrivenEach(instances))
        return "instance-status.turbo-stream"
    }

    private fun dataDrivenEach(stream: Flow<Any>) = ReactiveDataDriverContextVariable(stream, 1)
}
