/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

// #ProgressiveEnhancement
(window['EventSource'] && window['Turbo']) ?
  Turbo.connectStreamSource(new EventSource('/status.stream')) :
  console.warn('Turbo Streams over SSE not available');
