/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

const zeroPad = n => `${n < 10 ? '0' : ''}${n}`;

function updateClock() {
  const now = new Date();
  const hh = zeroPad(now.getUTCHours());
  const mm = zeroPad(now.getUTCMinutes());
  const ss = zeroPad(now.getUTCSeconds());
  let clock = document.getElementById('clock');
  clock.innerHTML = `${hh}:${mm}:${ss}&nbsp;UTC`;
  clock.setAttribute('datetime', now.toISOString());
}

// #ProgressiveEnhancement
document.getElementById('status_frame_load').click();

updateClock();
setInterval(updateClock, 1000);
