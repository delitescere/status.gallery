/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

package gallery.status.service

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import test.BaseTest
import test.EitherAssert.Companion.assertThat
import java.net.URL
import kotlinx.coroutines.*

class ObserveUrlIntegrationTest : BaseTest() {
    private fun check(url: String) = runBlocking { ObserveUrl.check(URL(url), 5000u) }

    @BeforeEach
    fun setTimeout() {
        System.setProperty("observer.timeout", "2000")
    }

    @Test
    fun `https response 200 should succeed`() {
        assertThat(check("https://httpstat.us/200")).isRight
    }

    @Test
    fun `https response 405 should succeed`() {
        assertThat(check("https://httpstat.us/405")).isRight
    }

    @Test
    fun `https response 500 should fail`() {
        assertThat(check("https://httpstat.us/500")).isLeft
    }
}
