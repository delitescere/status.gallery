/*
 * Copyright © 2021. Joshua A. Graham https://status.gallery/. See LICENSE.txt for usage rights.
 */

rootProject.name = "status.gallery"

// get kotlinVersion from gradle.properties into build.gradle.kts plugins section without duplicating
pluginManagement {
    val kotlinVersion: String by settings
    plugins {
        id("org.jetbrains.kotlin.jvm").version(kotlinVersion)
        id("org.jetbrains.kotlin.plugin.spring").version(kotlinVersion)
    }
}
