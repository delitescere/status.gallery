# 6. Design with low JavaScript frontend

Date: 2021-09-01

## Status

Accepted

Consequently [7. Use Hotwire](0007-use-hotwire.md)

## Context

JavaScript is built for updating the DOM when small client-side state transitions with simple logic can do it to improve user experience. Nothing 
else.

It is 2021. We have multi-channel, low-latency, highly concurrent, asynchronous, on-demand, only-what’s-needed, server-pushable content needs 
that can be 
solved without transmitting XML or JSON and translating that into DOM updates. Instead, we can transmit computed changes from the server as HTML 
and directly update the DOM with that. No "single-page app" required.

See original `#LowJS` [article](https://delitescere.medium.com/hotwire-html-over-the-wire-2c733487268c) by Josh Graham.

## Decision

* Use a "Low JS" approach.
* Keep the browser-side code as declarative as possible.
* Use progressive enhancement to fall back to workable interactions 
without JavaScript being available.
* Strive for Accessibility Guideline conformance. 

## Consequences

I avoid the special kind of insanity induced by working with JavaScript.
