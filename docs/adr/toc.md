# Architecture Decision Records

* [1. Record architecture decisions](0001-record-architecture-decisions.md)
* [2. Design as a Reactive system](0002-design-as-a-reactive-system.md)
* [3. Use Kotlin Coroutines](0003-use-kotlin-coroutines.md)
* [4. Use Spring WebFlux](0004-use-spring-webflux.md)
* [5. Use Kotlin Flows](0005-use-kotlin-flows.md)
* [6. Design with low JavaScript frontend](0006-design-with-low-javascript-frontend.md)
* [7. Use Hotwire](0007-use-hotwire.md)
