# 7. Use Hotwire

Date: 2021-09-01

## Status

Accepted

Consequence of [6. Design with low JavaScript frontend](0006-design-with-low-javascript-frontend.md)

## Context

I seriously explored [HTMX](https://htmx.org/) for this app. It is an neat little library to apply dynamic HTML capabilities to any element without
writing your own JavaScript. I didn't use HTMX due to only very basic handling of SSE that I couldn't work around for this app, but I expect this will be improved over time and it will be an excellent choice.

I also looked at Github's [Catalyst](https://github.github.io/catalyst/) but it still entails altogether too much client-side logic.

## Decision

Use [Hotwire](https://hotwired.dev).

## Consequences

Happiness for developers. Happiness for users.
