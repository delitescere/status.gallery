# 3. Use Kotlin Coroutines

Date: 2021-09-01

## Status

Accepted

Consequence of [2. Design as a Reactive system](0002-design-as-a-reactive-system.md)

Compatible with [4. Use Spring WebFlux](0004-use-spring-webflux.md)

Compatible with [5. Use Kotlin Flows](0005-use-kotlin-flows.md)

## Context

[Kotlin Coroutines](https://kotlinlang.org/docs/coroutines-overview.html) provide language-level support for asynchronous and non-blocking 
operations, as well as allowing for abstractions like Actors and straightforward expressions of concurrency.

## Decision

Use Kotlin Coroutines for services, actors, concurrent logic, and asynchronous operations.

## Consequences

Chosen instead of multi-threadeded programming or even of the more common Java concurrency mechanisms.

Some portions of the API to _**test**_ Kotlin Coroutines (as at time of writing) are in late experimental state. The production code APIs, however, 
have been out of experimental state for many versions.

Debugging assistance for coroutines is provided in IntelliJ.
