# 4. Use Spring WebFlux

Date: 2021-09-01

## Status

Accepted

Consequence of [2. Design as a Reactive system](0002-design-as-a-reactive-system.md)

Compatible with [3. Use Kotlin Coroutines](0003-use-kotlin-coroutines.md)

Compatible with [5. Use Kotlin Flows](0005-use-kotlin-flows.md)

## Context

Spring WebFlux is the non-blocking web framework in the Spring [Reactive Stack](https://spring.io/reactive). It is built on [Project Reactor](https://projectreactor.io/) and 
works smoothly with Kotlin Coroutines.

## Decision

Use Spring WebFlux as the web framework.

## Consequences

More documentation and samples exist for the traditional Spring MVC servlet-based web framework.
