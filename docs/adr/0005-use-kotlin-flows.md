# 5. Use Kotlin Flows

Date: 2021-09-01

## Status

Accepted

Consequence of [2. Design as a Reactive system](0002-design-as-a-reactive-system.md)

Compatible with [3. Use Kotlin Coroutines](0003-use-kotlin-coroutines.md)

Compatible with [4. Use Spring WebFlux](0004-use-spring-webflux.md)

## Context

There are several libraries on the JVM to choose from for building a reactive system that integrate well with Spring WebFlux and interoperate well 
with Kotlin and its coroutines.

Roman Elizarov has a comprehensive set of [articles](https://elizarov.medium.com/reactive-streams-and-kotlin-flows-bfd12772cda4) talking about Kotlin Flows, comparing them other approaches, design goals, outcomes, and 
links to plenty 
of further reading. 

Although Spring WebFlux is built on Project Reactor, the newer [Kotlin Flows](https://kotlinlang.org/docs/flow.html) are more idiomatic to Kotlin 
and provide a number of benefits to simplifying code.

## Decision

Use a Kotlin Flow wherever possible in preference to Flux/Mono.

## Consequences

Kotlin Flows are the newest player in the reactive Java space. The interop with Spring WebFlux works well currently, but may drift or lag over time.
