# 2. Design as a Reactive system

Date: 2021-09-01

## Status

Accepted

Consequently [3. Use Kotlin Coroutines](0003-use-kotlin-coroutines.md)

Consequently [4. Use Spring WebFlux](0004-use-spring-webflux.md)

Consequently [5. Use Kotlin Flows](0005-use-kotlin-flows.md)

## Context

Given the app will be primarily gathering information from an unknown number of distributed systems in an unknown topology, calculating health status,
and relaying that calculation to an unknown number of distributed clients (web browsers) in an unknown topology, a traditional "polling" approach with
shared mutable state, locks, and other blocking operations do not suit the problem domain.

Furthermore, it is expected that a reactive system will be more resilient to intermittent failure and use fewer system resources than traditional
approaches.

## Decision

The high-level design goals are:

* A fully reactive system that doesn't involve shared mutable state
* If there is shared mutable state, it is abstracted away as an implementation detail of the reactive toolkit, preferably using lock- and wait-free
  data structures
* Because calculating the current health status of each instances of a monitored system potentially involves looking at the past several observations,
  it needs to be captured even if "nobody is watching"
* Use tests and a REPL to drive the design, particularly of the reactive portions of the system
* Despite the cognitive load of multiple asynchronous, non-blocking, moving parts the reactive stream processing code should be fairly easy to
  follow (even if the underlying library mechanisms were not)

## Consequences

Reactive programming concepts and syntax are not mainstream and unfamiliar to most. Of the available toolkits and abstractions, selections should be
made which reduce cognitive load without compromising the reactive nature of the system.   
