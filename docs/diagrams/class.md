The package dependency graph.

```mermaid
classDiagram
    config ..> service
    service ..> domain
    domain *.. domain_event
    domain *.. domain_flow
    controller ..> service
    controller ..> viewmodel
```

A simplified class dependency graph.

```mermaid
classDiagram
    StatusController ..> ObserveeSupervisor
    ObserveeSupervisor o-- Observee
    Observee *-- ObserveeHealthFlow
    ObserveeHealthFlow *-- ObserveeHealthEvent
    ObserveeHealthEvent ..> HealthStatus
    ObserveeHealthFlow ..> ToleranceCalculator
    ToleranceCalculator ..> HealthStatus
    Observee o-- ObserveeInstance
    ObserveeInstance *-- InstanceHealthFlow
    InstanceHealthFlow *-- InstanceHealthEvent
    InstanceHealthEvent ..> HealthStatus
    InstanceHealthFlow ..> HealthCalculator
    InstanceHealthFlow *-- ObserveUrlEvent
    HealthCalculator ..> HealthStatus
    HealthCalculator ..> ObserveUrlEvent
    ObserveeInfo ..> ObserveeHealthFlow
    InstanceInfo ..> InstanceHealthFlow
    AbstractStatusInfo ..> HealthStatus
    AbstractStatusInfo <|-- ObserveeInfo
    AbstractStatusInfo <|-- InstanceInfo
    StatusController ..> ObserveeInfo
    StatusController ..> InstanceInfo
```
